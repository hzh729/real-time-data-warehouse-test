package com.gfscold.trans.common.constant;

import lombok.Data;

@Data
public class RecordDeleteLog {
    String schemaName ; //数据库名
    String tableName ;//表名
    Long recordId ;//记录id
    String recordValue ;//删除前记录
    String deleteTime ;//删除时间
    Boolean syncProcState ;//同步状态
}
