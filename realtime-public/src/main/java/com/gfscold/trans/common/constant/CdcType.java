package com.gfscold.trans.common.constant;

import lombok.Builder;
import lombok.Data;
import org.json.JSONObject;
@Builder
@Data
public class CdcType {
    String beforeId;
    String before;
    String after;
    String db;
    String table;
    String op;
    String ts_ms;
    String transaction;
}
