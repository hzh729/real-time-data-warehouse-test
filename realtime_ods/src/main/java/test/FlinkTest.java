package test;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;

import static org.apache.flink.streaming.api.environment.CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION;

public class FlinkTest {
    public static void main(String[] args) {
        int flinkPort = 10010;
        Configuration conf = new Configuration();
        conf.setInteger("rest.port", flinkPort);
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);


        // 1.4 状态后端及检查点相关配置
        // 1.4.1 设置状态后端
        env.setStateBackend(new HashMapStateBackend());

        // 1.4.2 开启 checkpoint
        env.enableCheckpointing(50000);
        // 1.4.3 设置 checkpoint 模式: 精准一次
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        // 1.4.4 checkpoint 存储
        env.getCheckpointConfig().setCheckpointStorage("file:\\C:\\check");
        // 1.4.5 checkpoint 并发数
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
        // 1.4.6 checkpoint 之间的最小间隔
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(5000);
        // 1.4.7 checkpoint  的超时时间
        env.getCheckpointConfig().setCheckpointTimeout(10000);
        // 1.4.8 job 取消时 checkpoint 保留策略
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(RETAIN_ON_CANCELLATION);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);
//        String connector = "mysql-cdc";
//        String hostname = "m3q8uro3hzqaa2g4uhe6.rwlb.rds.aliyuncs.com";
//        int port = 3306;
//        String username = "zhhuang4";
//        String password = "HZfR1cqrRcZC^";
//        String databaseName = "gfs_wms_bms";
//        String tableName = "customer_operation_config";
        String connector = "mysql-cdc";
        String hostname = "10.33.146.51";
        String port = "5189";
        String username = "zhhuang4";
        String password = "Gfs.2024!";
        String databaseName = "dwtest";
        String tableName = "test_table";

//        String createSql = "CREATE TABLE customer_operation_config(\n" +
//                "id int,\n" +
//                "warehouse_code String,\n" +
//                "customer_id int,\n" +
//                "customer_code String,\n" +
//                "customer_name String,\n" +
//                "operation String,\n" +
//                "operation_ts TIMESTAMP_LTZ(3) METADATA FROM 'op_ts' VIRTUAL,\n" +
//                "PRIMARY KEY (id) NOT ENFORCED\n" +
//                ") \n" ;
        String createSql = "CREATE TABLE IF NOT EXISTS test_table (  \n" +
                "    id INT,  \n" +
                "    name VARCHAR(20),  \n" +
                "    age INT,\n" +
                "    create_time TIMESTAMP,\n" +
                "    operation_ts TIMESTAMP_LTZ(3) METADATA FROM 'op_ts' VIRTUAL,\n" +
                "    PRIMARY KEY (id,create_time) NOT ENFORCED\n" +
                ")";
        String withSql =
                "WITH (\n" +
                "'connector' = '" + connector + "',\n" +
                "'hostname' = '" + hostname + "',\n" +
                "'port' = '" + port + "',\n" +
                "'username' = '" + username + "',\n" +
                "'password' = '" + password + "',\n" +
                "'database-name' = '" + databaseName + "',\n" +
                "'table-name' = '" + tableName + "',\n" +
                "'debezium.snapshot.mode' = 'initial' " +
                ");";
//        executeSQL(env,tableEnv, createSql, withSql, tableName);
        tableEnv.executeSql("CREATE TABLE IF NOT EXISTS edi_sys_user (\n" +
                "    `user_id` BIGINT NOT NULL,\n" +
                "    `username` STRING COMMENT '用户名',\n" +
                "    `password` STRING COMMENT '密码',\n" +
                "    `created_time` STRING COMMENT '创建时间',\n" +
                "    `remark` STRING COMMENT '公司',\n" +
                "    `status` BOOLEAN COMMENT '状态  0：禁用   1：正常',\n" +
                "    `account_non_expired` BOOLEAN,\n" +
                "    `account_non_locked` BOOLEAN,\n" +
                "    `credentials_non_expired` BOOLEAN,\n" +
                "    PRIMARY KEY ( `user_id` ) NOT ENFORCED\n" +
                ") WITH (\n" +
                "    'connector' = 'mysql-cdc'\n" +
                "    ,'hostname' = 'mr-kw25r6ycmmao28rnqa.rwlb.rds.aliyuncs.com'\n" +
                "    ,'port' = '3306'\n" +
                "    ,'username' = 'zhhuang4'\n" +
                "    ,'password' = 'HZfR1cqrRcZC^'\n" +
                "    ,'database-name' = 'gfs_edi'\n" +
                "    ,'table-name' = 'edi_sys_user'\n" +
                "    ,'debezium.snapshot.mode' = 'initial')" );
        tableEnv.executeSql("select * from edi_sys_user").print();

    }
    static void executeSQL(StreamExecutionEnvironment env, StreamTableEnvironment tableEnv, String createSql, String withSql, String tableName){
        tableEnv.executeSql(createSql + withSql);

        Table table =
                tableEnv.sqlQuery("SELECT \n" +
                "\tid\n" +
                "\t,DATE_FORMAT(create_time, 'yyyy-MM-dd') create_time\n" +
                "\t,name\n" +
                "\t,age\n" +
                "\t,CASE \n" +
                "\t\tWHEN YEAR(operation_ts) = 1970 THEN NULL\n" +
                "\t\tELSE DATE_FORMAT(operation_ts, 'yyyy-MM-dd HH:mm:ss')\n" +
                "\tEND AS  modified_time\n" +
                "FROM test_table");


//        DataStream<Tuple2<Boolean, Row>> stream = tableEnv.toRetractStream(table,Row.class);
//        stream.print();
//        //写出到Doris
        tableEnv.executeSql("CREATE TABLE test_tab (  \n" +
                "    `id` INT,  \n" +
                "    `create_time` String,  -- 更正了列名中的拼写错误\n" +
                "    `name` String,  \n" +
                "    `age` INT,       \n" +
                "    `modified_time` String  \n" +
                ") \n" +
                "with (\n" +
                "\t'connector' = 'doris',\n" +
                "\t'fenodes' = '172.19.2.250:8030',\n" +
                "\t'username' = 'zhhuang4',\n" +
                "\t'password' = 'HZfR1cqrRcZC^',\n" +
                "\t'table.identifier' = 'test_dw.test_tab',\n" +
                "\t'sink.properties.format' = 'json',\n" +
                "\t'sink.enable-2pc' = 'false',\n" +
                "\t'sink.properties.read_json_by_line' = 'true',\n" +
                "\t'sink.buffer-size' = '6',\n" +
                "\t'sink.buffer-count' = '4' \n" +
                ")");
        table.executeInsert("test_tab");
        // 4、提交任务
//        try {
//            env.execute("Flinksql");
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }

    }
}
